# Blog PHP 
Ce projet est un projet d'études ou il nous à été demandé de réaliser un blog en PHP sans framework. 

# Fonctionnalités

- [x] Inscription utilisateur
- [x] Connexion utilisateur
- [x] Profil utilisateur
- [x] Créer un article
- [x] Voir un article
- [x] Voir tous les articles
- [x] Voir les articles par catégorie
- [x] Commenter un article
- [] Répondre à un commentaire


# Installation

1. Télécharger le projet
2. Importer la base de données blog.sql
3. Modifier le fichier config/config.php avec vos informations de connexion à la base de données
4. Lancer le serveur PHP avec la commande 
5. Se rendre sur http://localhost:8000
6. vous pouvez commencer a utiliser le blog

Make Sure Apache and MariaDB (or MySQL) is running.

1. Setup MySQL -- config/config.php, it has been set to defaults as you would get in fresh XAMPP install. Change if not the case.
2. Import database -- blog.sql, go to PHPMyAdmin and import.
3. HTDocs folder -- put the files (or clone the repo) in the root of htdocs folder.

Finally, go to localhost to run the app.

