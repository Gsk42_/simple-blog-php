<?php foreach($posts as $post) { 
 
  $content = $post['Content'];
  if(strlen($content) > 100){
    $content = substr($content, 0, 100);
    $post['Content'] = $content . "...";
  }

  
  ?>
	<div class="post my-5">
		<h3><a href="<?php echo ROOT_URL ?>post.php?id=<?php echo $post['ID'] ?>"><?php echo $post['Title'] ?></a></h3>
		<small> Posted on <?php echo $post['Timestamp'] ?> by <?php echo $post['nom'] ?></small>
		<p><?php echo $post['Content'] ?></p>
	</div>
<?php } ?>

 
 
 
<button onclick="openModal()">Create Post</button>

<script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
}
</script>

 
<!-- Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <form action="./config/cp.php" method="post">
		<div class="form-group">
			<label for="title">Title</label>
			<input type="text" name="title" class="form-control" >
		</div>
		<div class="form-group">
			<input type="hidden" name="author" class="form-control" value="<?php echo $_SESSION["user"]["id"]; ?>">
		</div>
		<div class="form-group">
			<label for="content">Content</label>
			<textarea name="content" class="form-control" ></textarea>
		</div>
     
<select name="category" class="form-control">
  
	<?php foreach($cat as $category) { ?>
		<option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
	<?php } ?>
</select>

		<input type="submit" name="submit" class="btn btn-primary">
    <input type="hidden" name="action" value="login_form">
	</form>
  </div>

</div>

<script>
// Get the modal
var modal = document.getElementById("myModal");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

<style>
 
.modal {
  display: none; 
  position: fixed; 
  z-index: 1; 
  left: 0;
  top: 0;
  width: 100%; 
  height: 100%; 
  overflow: auto; 
  background-color: rgb(0,0,0); 
  background-color: rgba(0,0,0,0.4); 
}

.modal-content {
  background-color: #fefefe;
  margin: 15% auto; 
  padding: 20px;
  border: 1px solid #888;
  width: 80%; 
}

.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.form-group {
  margin-bottom: 1rem;
}

 
.post {
  border: 1px solid #ccc;
  padding: 10px;
  margin-bottom: 10px;
}

.post h3 {
  margin-top: 0;
}

.post small {
  display: block;
  margin-bottom: 10px;
}

.modal select {
  margin-bottom: 10px;
}

.modal input[type="submit"] {
  margin-top: 10px;
}



</style>



 

