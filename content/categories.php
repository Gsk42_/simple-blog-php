<?php foreach($cat as $post) { ?>
	<div class="post my-5">
		<h3><a href="<?php echo ROOT_URL ?>post_by_cat.php?id=<?php echo $post['id'] ?>"><?php echo $post['name'] ?></a></h3>
	</div>
<?php } ?>
 
<button onclick="openModal()">Creer une catégorie</button>

<script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
}
</script>

 
<style>
.modal {
  display: none; 
  position: fixed; 
  z-index: 1; 
  padding-top: 100px; 
  left: 0;
  top: 0;
  width: 100%; 
  height: 100%; 
  overflow: auto; 
  background-color: rgb(0,0,0); 
  background-color: rgba(0,0,0,0.4); 
}

.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 80%;
}

.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
</style>

<div id="myModal" class="modal">

  <div class="modal-content">
    <span class="close">&times;</span>
     
<form action="<?php echo ROOT_URL ?>/config/create_category.php" method="POST">
  <label for="name">Nom de la catégorie:</label><br>
  <input type="text" id="name" name="name"><br>
  <input type="submit" value="Creer">
</form>

  </div>

</div>

<script>
var modal = document.getElementById("myModal");

var span = document.getElementsByClassName("close")[0];

span.onclick = function() {
  modal.style.display = "none";
}

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

