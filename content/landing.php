	<main role="main" class="inner cover">
	  <h1 class="cover-heading">Welcome to BlogOne</h1>
	  	<p class="lead">This is a great blog site!</p>
		<p class="lead"><a href="<?php echo ROOT_URL ?>blog.php" class="btn btn-lg btn-secondary">View Blogs</a></p>
	</main>

	<div class="container">
		<h1>Liste des utilisateurs</h1>
		<?php 

			echo $_SESSION['user']['nom'];

			?>
		<table class="table">
			<thead>
				<tr>
					<th>Nom</th>
					<th>Prénom</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($users as $user) : ?>
					<tr>
						<td><?php echo $user['nom']; ?></td>
						<td><?php echo $user['prenom']; ?></td>
						<td>
														
							<form action="<?php echo ROOT_URL ?>config/login.php" method="post">
								<input type="hidden" name="id" value="<?php echo $user['id']; ?>">
								<button type="submit" class="btn btn-primary">Se connecter</button>
							</form>

						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>


		
	<form action="<?php echo ROOT_URL ?>/config/register.php" method="post">
		<div class="form-group">
			<label for="nom">Nom:</label>
			<input type="text" class="form-control" id="nom" name="nom" required>
		</div>
		<div class="form-group">
			<label for="prenom">Prenom:</label>
			<input type="text" class="form-control" id="prenom" name="prenom" required>
		</div>
		<div class="form-group">
			<label for="password">Mot de passe:</label>
			<input type="password" class="form-control" id="password" name="password" required>
		</div>
		<button type="submit" class="btn btn-primary">Créer un compte</button>
	</form>
