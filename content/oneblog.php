		<div class="post my-5">
			<h3><a href="<?php echo ROOT_URL ?>post.php?id=<?php echo $post['ID'] ?>"><?php echo $post['Title'] ?></a></h3>
			<small> Posted on <?php echo $post['Timestamp'] ?> by <?php echo $post['nom'] ?></small>
			<p><?php echo $post['Content'] ?></p>
		</div>

			
		<!-- Add a comment section -->
		<div class="comments my-5">
			<h4>Comments</h4>
			<?php foreach($comments as $comment): ?>
				<div class="comment">
					<small>Commented on <?php echo $comment['Timestamp'] ?> by <?php echo $comment['nom']," ", $comment['prenom'] ?></small>
					<p><?php echo $comment['message'] ?></p>
				</div>
			<?php endforeach; ?>
			<!-- Add a form to submit new comments -->
			<form method="POST" action="<?php echo ROOT_URL ?>/config/add_comment.php">
				<input type="hidden" name="post_id" value="<?php echo $post['ID'] ?>">
				<input type="hidden" name="user_id" value="<?php echo $_SESSION['user']['id'] ?>">
				<div class="form-group">
					<label for="comment">Comment:</label>
					<textarea class="form-control" id="comment" name="message" required></textarea>
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>

